# dictzip

DictZip, GZip random access compression format(.dz), access library for Java

## Usage

A dictzip library is published on MavenCentral. When you use it with a Gradle build system,
you can add a dependency like as follows.

```
dependencies {
    implementation 'io.github.dictzip:dictzip:1.0.0'
}
```

## Contribution

As usual of other projects hosted on GitHub, DictZip for java also welcomes you to fork
a source and send modification as a Pull Request.
It is recommended to post an issue before sending a patch.

## Copyright

- Copyright (C) 2001–2004 Ho Ngoc Duc
- Copyright (C) 2016–2024 Hiroshi Miura

Some parts are coming from a part of jdictd 1.5 on java.

## License

Please see COPYING.
