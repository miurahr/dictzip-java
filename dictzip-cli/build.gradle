import org.apache.tools.ant.filters.ReplaceTokens

plugins {
    id 'application'
    id 'jacoco'
    id 'checkstyle'
}

application {
    mainClass = 'org.dict.zip.cli.Main'
    applicationName = 'dictzip'
}

version = rootProject.version

def props = project.file("src/main/resources/org/dict/zip/Version.properties")
tasks.register('writeVersionFile') {
    def folder = props.getParentFile()
    if (!folder.exists()) {
        folder.mkdirs()
    }
    props.delete()
    props.text = "version=" + project.version
}
jar.dependsOn("writeVersionFile")

repositories {
    mavenCentral()
}

dependencies {
    implementation project(':dictzip-lib')
    implementation(libs.jetbrains.annotations)
    implementation(libs.picocli)
    testImplementation(libs.commons.io)
    testImplementation(libs.junit.jupiter)
    testImplementation project(':northside-io')
}

test {
    useJUnitPlatform()
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(11)
        vendor = JvmVendorSpec.ADOPTIUM
    }
}

compileJava {
    options.compilerArgs += ["-Aproject=${project.group}/${project.name}"]
}

tasks.register('mandoc', Copy) {
    from "doc/dictzip.1.in"
    into layout.buildDirectory.file("docs/man")
    rename { String fileName ->
        fileName.replace('dictzip.1.in', 'dictzip.1')
    }
    filter ReplaceTokens, tokens: [copyright: projectYears, version: project.version]
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}

distTar {
    compression = Compression.GZIP
}
distTar.dependsOn mandoc

distributions {
    main {
        contents {
            from(tasks.mandoc.outputs) {
                into 'docs/man/man1'
            }
        }
    }
}

checkstyle {
    ignoreFailures = true
    toolVersion = libs.versions.checkstyle.get()
}
