module tokyo.northside.dictzip.cli {
    requires java.base;
    requires info.picocli;
    requires org.jetbrains.annotations;
    requires tokyo.northside.dictzip.lib;
}