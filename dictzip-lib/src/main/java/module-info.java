module tokyo.northside.dictzip.lib {
    requires java.base;
    requires org.jetbrains.annotations;
    exports org.dict.zip;
}