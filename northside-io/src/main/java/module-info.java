module tokyo.northside.io {
    requires java.base;
    requires org.jetbrains.annotations;
    requires org.apache.commons.io;
    exports tokyo.northside.io;
}